﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePattern;

// Базовый абстрактный класс для некоторого контента в условной социальной сети
// от Content наследуют Image и Audio
internal abstract class Content
{
    public string? Name { get; set; }

    protected Content(string? name)
    { 
        Name = name; 
    }

    protected Content(Content source)
    {
        Name = source.Name;
    }

    public abstract Task PlayAsync();

    public override string ToString() => Name ?? "";
}
