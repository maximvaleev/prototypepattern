﻿//Реализуем паттерн "Прототип"
//Цель:

//Создать иерархию из нескольких классов, в которых реализованы методы клонирования объектов по шаблону проектирования "Прототип".

//Описание/Пошаговая инструкция выполнения домашнего задания:

//    Придумать и создать 3-4 класса, которые как минимум дважды наследуются и написать краткое описание текстом.
//    Создать свой дженерик интерфейс IMyCloneable для реализации шаблона "Прототип".
//    Сделать возможность клонирования объекта для каждого из этих классов, используя вызовы родительских конструкторов.
//    Составить тесты или написать программу для демонстрации функции клонирования.
//    Добавить к каждому классу реализацию стандартного интерфейса ICloneable и реализовать его функционал через уже созданные методы.
//    Написать вывод: какие преимущества и недостатки у каждого из интерфейсов: IMyCloneable и ICloneable.

namespace PrototypePattern;

/* Описание классов:
 * Content - абстрактный класс родитель для всего контента. Наследники: Audio и Image
 * Audio - аудиозапись. Класс родитель для Music и AudioPodcast
 * EAudioQuality - перечисление качества аудиозаписи
 * MusicInfo - класс с метатэгами музыкального трека
 * Image - изображение.
 * ImageInfo - класс с метатегами изображения
*/ 

// Прошу прощения что так много кода получилось, так уж получилось)

/* Преимущества и недостатки интерфейсов:
 * Свой нужно создавать, а готовый уже есть
 * Свой обобщенный - лучше типобезопасность и не нужно приведение типов
 * Свой может вызвать замешательство у читателя кода, непонятно зачем он нужен, если есть готовый
 */

internal class Program
{
    static async Task Main()
    {
        // Создать несколько единиц контента
        (Audio audioInit, Music musicInit, AudioPodcast podcastInit, Image imageInit) = CreateContents();
        // Скопировать контент через IMyCloneable метод
        Audio audioCopy1 = audioInit.DeepCopy();
        Music musicCopy1 = musicInit.DeepCopy();
        AudioPodcast podcastCopy1 = podcastInit.DeepCopy();
        Image imageCopy1 = imageInit.DeepCopy();
        // Скопировать контент через ICloneable метод
        Audio audioCopy2 = (Audio)audioInit.Clone();
        Music musicCopy2 = (Music)musicInit.Clone();
        AudioPodcast podcastCopy2 = (AudioPodcast)podcastInit.Clone();
        Image imageCopy2 = (Image)imageInit.Clone();
        // Изменить копии
        ChangeContents(ref audioCopy1, ref musicCopy1, ref podcastCopy1, ref imageCopy1);
        ChangeContentsAlternative(ref audioCopy2, ref musicCopy2, ref podcastCopy2, ref imageCopy2);
        // Показать что получилось
        await Console.Out.WriteLineAsync("--- Изначальные версии контента ---");
        PrintContents(audioInit, musicInit, podcastInit, imageInit);
        await Console.Out.WriteLineAsync("--- Первые копии контента ---");
        PrintContents(audioCopy1, musicCopy1, podcastCopy1, imageCopy1);
        await Console.Out.WriteLineAsync("--- Вторые копии контента ---");
        PrintContents(audioCopy2, musicCopy2, podcastCopy2, imageCopy2);
    }

    private static (Audio, Music, AudioPodcast, Image) CreateContents()
    {
        Audio au = new(
            "Рык африканского льва",
            new TimeSpan(0, 0, 1),
            EAudioQuality.Low);
        Music mu = new(
            "Le Solidat",
            new TimeSpan(0, 0, 2),
            EAudioQuality.High,
            new MusicInfo(
                "Le Solidat",
                "Ismael Rivera",
                "Con Todos...",
                1973));
        AudioPodcast ap = new(
            "Аэростат №45",
            new TimeSpan(0, 0, 3),
            EAudioQuality.Standart,
            "БГ - Аэростат №45 - Donovan");
        Image im = new(
            "Афириканский лев на рассвете",
            1024,
            768,
            new ImageInfo(
                DateTimeOffset.Now,
                true,
                9.0845755,
                8.6742524));

        return (au, mu, ap, im);
    }

    private static void ChangeContents(ref Audio audio, ref Music music, ref AudioPodcast podcast, ref Image image)
    {
        audio.Name += "_1";
        audio.Duration *= 1.1;
        audio.Quality = EAudioQuality.High;
        
        music.Name += "_1";
        music.Duration *= 1.1;
        music.Quality = EAudioQuality.Low;
        if (music.MusicInfo is not null)
        {
            music.MusicInfo.Title = "Gates Unveiled";
            music.MusicInfo.Author = "Pachira";
            music.MusicInfo.Album = "Earthbound";
            music.MusicInfo.Year = 2020;
        }

        podcast.Name += "_1";
        podcast.Duration *= 1.1;
        podcast.Quality = EAudioQuality.High;
        podcast.Description += " copy_1";

        image.Name += "_1";
        image.Width = 2556;
        image.Height = 1080;
        if (image.ImageInfo is not null) 
        {
            image.ImageInfo.DateCreated = DateTime.Now.AddDays(-1d);
            image.ImageInfo.Latitude += 1;
            image.ImageInfo.Longitude += 1;
            image.ImageInfo.IsCompressed = !image.ImageInfo.IsCompressed;
        }
    }

    private static void ChangeContentsAlternative(ref Audio audio, ref Music music, ref AudioPodcast podcast, ref Image image)
    {
        audio.Name += "_2";
        audio.Duration *= 0.9;
        audio.Quality = EAudioQuality.Standart;

        music.Name += "_2";
        music.Duration *= 0.9;
        music.Quality = EAudioQuality.Standart;
        if (music.MusicInfo is not null)
        {
            music.MusicInfo.Title = "Playing in the sun";
            music.MusicInfo.Author = "Beicoli";
            music.MusicInfo.Album = "Laika";
            music.MusicInfo.Year = 2023;
        }

        podcast.Name += "_2";
        podcast.Duration *= 0.9;
        podcast.Quality = EAudioQuality.Standart;
        podcast.Description += " copy_2";

        image.Name += "_2";
        image.Width = 640;
        image.Height = 480;
        if (image.ImageInfo is not null)
        {
            image.ImageInfo.DateCreated = DateTime.Now.AddDays(+1d);
            image.ImageInfo.Latitude -= 1;
            image.ImageInfo.Longitude -= 1;
            image.ImageInfo.IsCompressed = !image.ImageInfo.IsCompressed;
        }
    }

    private static void PrintContents(Audio audio, Music music, AudioPodcast podcast, Image image)
    {
        Console.WriteLine(audio);
        Console.WriteLine(music);
        Console.WriteLine(podcast);
        Console.WriteLine(image);
        Console.WriteLine();
    }
}
