﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePattern;

internal class Image : Content, IMyCloneable<Image>, ICloneable
{
    public uint Width { get; set; }
    public uint Height { get; set; }
    public ImageInfo? ImageInfo { get; set; }

    public Image(string? name, uint width, uint height, ImageInfo? imageInfo = null) : base(name)
    {
        Width = width;
        Height = height;
        ImageInfo = imageInfo;
    }

    protected Image(Image source) : base(source)
    {
        Width = source.Width;
        Height = source.Height;
        ImageInfo = source.ImageInfo?.DeepCopy();
    }

    public async override Task PlayAsync() => await Console.Out.WriteLineAsync($"Показано изображение \"{Name}\".");

    public void SaveToMyPictures() => Console.WriteLine($"Изображение \"{Name}\" сохранено в Мои Картинки");

    public Image DeepCopy() => new Image(this);

    public object Clone() => DeepCopy();

    public override string ToString() => "Изображение: " + base.ToString() + "; " + $"{Width}x{Height}; " + ImageInfo?.ToString();
}
