﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePattern;

// аудио-контент. В основном это базовый класс для более конкретных классов, но может быть инстанцирован и сам.
// от Audio наслудуют Music и AudioPodcast
internal class Audio : Content, IMyCloneable<Audio>, ICloneable
{
    public TimeSpan Duration { get; set; }
    public EAudioQuality Quality { get; set; }
    
    public Audio(string? name, TimeSpan duration, EAudioQuality quality = EAudioQuality.Standart) 
        : base(name)
    {
        Duration = duration;
        Quality = quality;
    }

    protected Audio(Audio source) : base(source)
    {
        Duration = source.Duration;
        Quality = source.Quality;
    }
    
    public async override Task PlayAsync()
    {
        await Console.Out.WriteLineAsync($"Начато проигрывание аудио \"{Name}\"...");
        await Task.Delay( Duration );
        await Console.Out.WriteLineAsync($"Завершено проигрывание аудио \"{Name}\".");
    }

    public Audio DeepCopy() => new Audio(this);

    public object Clone() => DeepCopy();

    public override string ToString() => "Аудиозапись: " + base.ToString() + "; " + Duration.ToString(@"mm\:ss") + ", " + Quality + " quality";
}
