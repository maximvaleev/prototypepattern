﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePattern;

internal enum EAudioQuality
{
    None = 0,
    Standart = 1,
    Low = 2,
    High = 3,
}
