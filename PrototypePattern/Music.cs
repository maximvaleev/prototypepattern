﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePattern;

internal class Music : Audio, IMyCloneable<Music>, ICloneable
{
    public MusicInfo? MusicInfo { get; set; }

    public Music(string? name, TimeSpan duration, EAudioQuality quality = EAudioQuality.Standart, MusicInfo? musicInfo = null) 
        : base(name, duration, quality)
    {
        MusicInfo = musicInfo;
    }

    protected Music(Music source) : base(source)
    {
        MusicInfo = source.MusicInfo?.DeepCopy();
    }

    public async override Task PlayAsync()
    {
        await Console.Out.WriteLineAsync($"Начато проигрывание музыки \"{Name}\"...");
        await Task.Delay(Duration);
        await Console.Out.WriteLineAsync($"Завершено проигрывание музыки \"{Name}\".");
    }

    public void AddToMyMusic() => Console.WriteLine($"Трек \"{Name}\" добавлен в Моя Музыка");

    public new Music DeepCopy() => new Music(this);

    public new object Clone() => DeepCopy();

    public override string ToString() => "Музыка: " + base.ToString() + "; " + MusicInfo?.ToString();
}
