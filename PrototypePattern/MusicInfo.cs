﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePattern;

// класс с метаданными музыкального трека
internal class MusicInfo : IMyCloneable<MusicInfo>
{
    public string? Title { get; set; }
    public string? Author { get; set; }
    public string? Album { get; set; }
    public int? Year { get; set; }

    public MusicInfo(string? title, string? author, string? album = null, int? year = null)
    {
        Title = title;
        Author = author;
        Album = album;
        Year = year;
    }

    public MusicInfo DeepCopy() => new MusicInfo(Title, Author, Album, Year);

    public override string ToString() => $"{Author} - {Title} ({Year})";
}
