﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePattern;

// класс с методанными изображения: когда и где снято и т.д.
internal class ImageInfo : IMyCloneable<ImageInfo>
{
    public DateTimeOffset? DateCreated {  get; set; }
    public double? Latitude { get; set; }
    public double? Longitude { get; set; }
    public bool? IsCompressed { get; set; }

    public ImageInfo(DateTimeOffset? dateCreated, bool? isCompressed, double? latitude = null, double? longitude = null)
    {
        DateCreated = dateCreated;
        Latitude = latitude;
        Longitude = longitude;
        IsCompressed = isCompressed;
    }

    public ImageInfo DeepCopy() => new ImageInfo(DateCreated, IsCompressed, Latitude, Longitude);

    public override string ToString() => $"{DateCreated} - Location: {Latitude:F6}, {Longitude:F6}";
}
