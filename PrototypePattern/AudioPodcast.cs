﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePattern;

internal class AudioPodcast : Audio, IMyCloneable<AudioPodcast>, ICloneable
{
    public string? Description { get; set; }

    public AudioPodcast(string? name, TimeSpan duration, EAudioQuality quality = EAudioQuality.Standart, string? description = null) 
        : base(name, duration, quality)
    {
        Description = description;
    }

    protected AudioPodcast(AudioPodcast source) : base(source) 
    {
        Description = source.Description;
    }

    public void AddToMyPodcasts() => Console.WriteLine($"Подкаст \"{Name}\" добавлен в Мои Подкасты");

    public async override Task PlayAsync()
    {
        await Console.Out.WriteLineAsync($"Начато проигрывание подкаста \"{Name}\"...");
        await Task.Delay(Duration);
        await Console.Out.WriteLineAsync($"Завершено проигрывание подкаста \"{Name}\".");
    }

    public new AudioPodcast DeepCopy() => new AudioPodcast(this);

    public new object Clone() => DeepCopy();

    public override string ToString() => "Подкаст: " + base.ToString() + "; " + Description;
}
